---
title:  "gqlite 1.0"
date:   2023-11-17
excerpt: First release, with SQLite backend, graph creation, editing and matching. Bindings for C/C++, Ruby, Python and more.
categories: news
---

This is the first release of GQLite with support for SQLite backend, and the basic features of OpenCypher Query Language. It can now execute queries on a single graph, called `default`. Creation, matching, editing queries are currently supported, more details available in the [documentation](/documentation/stable/opencypher).

GQLite 1.0 can be used in C, C++, Ruby, Python and Crystal program. QML bindings are coming via the [Cyqlops library](https://gitlab.com/cyloncore/Cyqlops/).

You can check our [installation instructions](/documentation/stable/installation).

