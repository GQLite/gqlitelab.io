---
title: GQLite Documentation (stable)
permalink: /documentation/stable/
redirect_from:
  - /stable
---

- [Installation](installation) instructions
- [API Documentation](api) for the different supported programming language
- [Open Cypher Query Language](opencypher) documentation of the implementation