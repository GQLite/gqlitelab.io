---
title: GQLite Documentation (dev)
permalink: /documentation/dev/
redirect_from:
  - /dev
---

- [Installation](installation) instructions
- [API Documentation](api) for the different supported programming language
- [Open Cypher Query Language](opencypher) documentation of the implementation