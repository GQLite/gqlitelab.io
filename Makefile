prepare:
	bundle install

test:
	bundle exec jekyll serve

fetch:
	git clone https://gitlab.com/GQLite/GQLite.git _includes/GQLite/stable
	git clone https://gitlab.com/GQLite/GQLite.git _includes/GQLite/dev

update:
	cd _includes/GQLite/stable; git pull; git checkout stable
	cd _includes/GQLite/dev; git pull; git checkout dev/1
