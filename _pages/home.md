---
layout: single
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/gqlite-home-page-feature.jpg
---

GQLite is a C++-language library, with a C interface, that implements a small, fast, self-contained, high-reliability, full-featured, Graph Query database engine. The data is stored in a [SQLite](https://sqlite.org) database, which the fasted and most used SQL database. This enable to achieve high performance and for application to combine Graph queries with traditional SQL queries.

The source code is available on gitlab: [https://gitlab.com/gqlite/gqlite](https://gitlab.com/gqlite/gqlite).

GQLite source code is license under the [MIT License](license) and is free to everyone to use for any purpose. 

The official repositories contains bindings/APIs for C, C++, Python and Ruby.

The library is still in its early stage, but it is now fully functional. Development effort has now slowed down and new features are added on a by-need basis. It supports a subset of [OpenCypher](https://opencypher.org/), and the intent is to also support ISO GQL in the future when it become available.

News
----

<div class="feature__wrapper">
   {% for post in site.posts limit:3 %}
   <div class="feature__item">
      <div class="archive__item">
         <div class="archive__item-body">
            <h3 class="archive__item-title"><a href="{{ site.baseurl }}{{ post.url}}" rel="permalink">{{ post.title }}</a></h3>
            <div class="archive__item-excerpt">
               <p>{{ post.excerpt | markdownify }}</p>
            </div>
         </div>
      </div>
   </div>
   {% endfor %}
</div>
